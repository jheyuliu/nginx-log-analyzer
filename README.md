## How to run scripts

Notice: These scripts have test on Ubuntu Server.

1. Count the total number of HTTP requests recorded by this access logfile

script: count_access_log_records_Version_1, count_access_log_records_Version_2

run script:

./count_access_log_records_Version_1 [access_log file]
./count_access_log_records_Version_2 [access_log file]

example:

./count_access_log_records_Version_1 access_log

2. Find the Top 10 hosts that made the most requests from 2019-06-10 00:00:00 up to and including 2019-06-19 23:59:59

script: count_access_log_top_hosts_records_Version_1

run script:

./count_access_log_top_hosts_records_Version_1 [access_log file] [top N records | default N is 10] [start time|time format is yyyy-mm-dd hh:mm:ss|default start time is 2019-06-10 00:00:00]   [end time|time format is yyyy-mm-dd hh:mm:ss|default end time is 2019-06-19 23:59:59]

example:

./count_access_log_top_hosts_records_Version_1 access_log
./count_access_log_top_hosts_records_Version_1 access_log 10
./count_access_log_top_hosts_records_Version_1 access_log 10 "2019-06-10 00:00:00" "2019-06-19 23:59:59"

3. Find the country that made the most requests (hint: use the source IP address as a start)

script: get_top_country_by_access_log_Version_1

run scripts:

./get_top_country_by_access_log_Version_1 access.log [top N records|default N is 1]"

example:

./get_top_country_by_access_log_Version_1 access.log 10